using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudiosFinal : MonoBehaviour
{
    public AudioClip[] soundClips;
    public AudioClip thirdAudioClip;
    public GameObject targetObject;
    private AudioSource audioSource;
    private bool isColliding = false;
    private int currentClipIndex = 0;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetKeyDown("e") && isColliding)
        {
            PlaySound();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Jugador"))
        {
            isColliding = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.CompareTag("Jugador"))
        {
            isColliding = false;
        }
    }

    private void PlaySound()
    {
        if (currentClipIndex < soundClips.Length)
        {
            audioSource.clip = soundClips[currentClipIndex];
            audioSource.Play();
            StartCoroutine(WaitForSoundToFinish());
        }
        else
        {
            // Mostrar el objeto específico
            targetObject.SetActive(true);

            // Reproducir el tercer audio
            audioSource.clip = thirdAudioClip;
            audioSource.Play();
        }
    }

    private IEnumerator WaitForSoundToFinish()
    {
        yield return new WaitForSeconds(audioSource.clip.length);
        currentClipIndex++;
        PlaySound();
    }
}
