using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    public AudioClip soundClip;
    private AudioSource audioSource;
    private bool isColliding = false;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetKeyDown("e") && isColliding)
        {
            
            PlaySound();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Jugador"))
        {
            isColliding = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.CompareTag("Jugador"))
        {
            isColliding = false;
        }
    }

    private void PlaySound()
    {
        audioSource.PlayOneShot(soundClip);
    }
}
