using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AparicionObjeto : MonoBehaviour
{
    public GameObject objetoAparecido; // Objeto que debe aparecer

    private void Start()
    {
        objetoAparecido.SetActive(false); // Desactiva el objeto al inicio
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("ObjetoDetectado"))
        {
            objetoAparecido.SetActive(true); // Activa el objeto al detectar el otro objeto
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("ObjetoDetectado"))
        {
            objetoAparecido.SetActive(false); // Desactiva el objeto al salir el otro objeto
        }
    }
}
