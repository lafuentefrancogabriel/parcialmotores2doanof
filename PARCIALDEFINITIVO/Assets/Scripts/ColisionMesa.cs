using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColisionMesa : MonoBehaviour
{
    public GameObject objectToDisappear;
    public GameObject objectToAppear;
    

    private bool isColliding = false;

    private void Update()
    {
        if (isColliding && Input.GetKeyDown(KeyCode.E))
        {
            objectToDisappear.SetActive(false);
            objectToAppear.SetActive(true);
        }
    }
    private void Start()
    {
        objectToAppear.SetActive(false);

    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Jugador"))
        {
            isColliding = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            isColliding = false;
        }
    }
}

