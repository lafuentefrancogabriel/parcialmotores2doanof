using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionAudio : MonoBehaviour
{
    public AudioClip audioClip; // El AudioClip que se reproducirá

    
    private void OnTriggerEnter(Collider other)
    { 
        if (other.CompareTag("Jugador"))
        {
            AudioSource.PlayClipAtPoint(audioClip, transform.position); // Reproducir el AudioClip en la posición del objeto
        }
    }
    
}
