using UnityEngine;
using UnityEngine.SceneManagement;

public class CambioDeEscena : MonoBehaviour
{
    public KeyCode changeSceneKey = KeyCode.E;  // Tecla utilizada para cambiar de escena
    public string targetSceneName;              // Nombre de la escena a la que se desea cambiar
    private bool isColliding = false;            // Variable para rastrear la colisi�n con el collider espec�fico

    private void Update()
    {
        // Verificar si se presiona la tecla asignada y si hay una colisi�n con el collider espec�fico
        if (Input.GetKeyDown(changeSceneKey) && isColliding)
        {
            ChangeScene();   // Llamar a la funci�n para cambiar de escena
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Verificar si el objeto colisiona con el collider espec�fico al entrar en contacto
        if (other.CompareTag("cambiadorEscena") && gameObject.CompareTag("Jugador"))
        {
            isColliding = true;    // Establecer la variable de colisi�n en true
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Verificar si el objeto deja de colisionar con el collider espec�fico al salir de contacto
        if (other.CompareTag("cambiadorEscena") && gameObject.CompareTag("Jugador"))
        {
            isColliding = false;   // Establecer la variable de colisi�n en false
        }
    }

    private void ChangeScene()
    {
        SceneManager.LoadScene(targetSceneName);   // Cambiar a la escena especificada
    }
}
